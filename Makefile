###############################################################################
# University of Hawaii, College of Engineering
# EE 367 
# Lab 03 - Client server Lab
#
# @file    Makefile
# @version 1.0
#
# @author EmilyPham <emilyn3@hawaii.edu>
# @date  20_JAN_2022 
###############################################################################

OBJ = server.o client.o 

all: client server client367 server367

367: client367 server367 

server.o: server.c
	g++ -c server.c

client.o: client.c
	g++ -c client.c

server.367o: server367.c
	g++ -c server367.c

client367.o: client367.c
	g++ -c client367.c

server: server.o 
	g++ -o server server.o 

client: client.o
	g++ -o client client.o

server367: server367.o 
	g++ -o server367ep server367.o 

client367: client367.o
	g++ -o client367ep client367.o

clean:
	rm -f *.o 
