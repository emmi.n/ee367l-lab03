/*
** client367.c 
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <arpa/inet.h>

#define PORT "3523" // the port client will be connecting to 
#define HOSTNAME "wiliki.eng.hawaii.edu" // the hostname of client 

#define MAXDATASIZE 100 // max number of bytes we can get at once 
#define MAXANSWERSIZE 2

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

// GIVEN: request string, 
// DOES: stores entered filename into 'filename'
void getFilename( char request[], char filename[] ) {
   int i = 0;
   int k = 0;

   for (; i < strlen(request); i++) {
      if (request[i] == ' ') {
         i++;
         break;
      }
   }
   if (i == strlen(request)) {
      printf("invalid command format\n");
      filename[0] = '\0';
      return;
   }

   for (int j = i; j != strlen(request); j++) {
      filename[k] = request[j];
      k++;
   }
   filename[k+1] = '\0';
}

// GIVEN:   hostname
// DOES:    trys to connect to server at given hostname
// RETURNS: sockfd for successful connections
// int connectTOserver( char* hostname ) {


int main(int argc, char *argv[])
{
	int sockfd, numbytes;  
	char buf[MAXDATASIZE];
	char request[MAXDATASIZE];
	char response[MAXDATASIZE];
	char filename[MAXDATASIZE];
   const char BASE_PATH[20] = "./Downloads/";
	struct addrinfo hints, *servinfo, *p;
	int rv;
	char s[INET6_ADDRSTRLEN];
	bool quit = false;
   bool cancel = false;
   bool decision = false;
   FILE* targetFile = NULL;
   char ans[] = " ";
   FILE* newFile = NULL;

	if (argc != 2) {
	    fprintf(stderr,"usage: client hostname\n");
	    exit(1);
	}

	while (!quit) {
      // reset flags
      cancel = false;
      decision = false;

      // reset request and filename buffer
      for (int i=0; request[i] != '\0'; i++) {
         request[i] = '\0';
         printf("%c", request[i]);
      }
      for (int i=0; filename[i] != '\0'; i++) {
         filename[i] = '\0';
         printf("%c", filename[i]);
      }

      // create path var and initialize
      char* path = (char*)malloc( sizeof(char) * (strlen(BASE_PATH)+strlen(filename)) );
      strcpy(path, BASE_PATH);

		// prompt user for command
		printf("Command (enter 'h' for help): ");
		//scanf("%[^\n]%*c", request);  // reads until '\n' then discards '\n' char
      fgets(request, MAXDATASIZE, stdin);
      request[strcspn(request, "\n")] = '\0';
		//printf("DEBUG:%s\n", request);

		// process command
		switch (request[0]) {
			case 'l':
				filename[0] = '\0';	//don't need a filename
            request[1] = '\0';
				break;
			case 'c':
				getFilename( request, filename );
            request[1] = '\0';
				break;
			case 'p':
				getFilename( request, filename );
            request[1] = '\0';
				break;
			case 'd':
				getFilename( request, filename );
            request[1] = '\0';

            // make "Downloads" dir if it doesn't exist
            if (!fork()) { // child to execute mkdir 
               execlp("/bin/mkdir", "mkdir", "Downloads", (char*)NULL);
               exit(0);
            }
            while( wait(NULL) > 0 );   // wait for child process to finish

            // append Download dir to filename
            strcat(path, filename);
            //printf("DEBUG: path = %s\n", path);
            
            // check if file exists
            targetFile = fopen(path, "r");
            if (targetFile != NULL) {
               printf("File exists. Replace file? (y/n): ");
               fgets( ans, MAXANSWERSIZE, stdin);
               while (getchar() != '\n') {}
               while (decision == false) {
                  if ((strcmp(ans,"N")==0) || (strcmp(ans,"n")==0)) {
                     printf("File will not be replaced. Canceling download...\n");
                     cancel = true;
                     decision = true;
                  } else if ((strcmp(ans,"Y")==0) || (strcmp(ans,"y")==0)) {
                     printf("File %s will be replaced...\n", filename);
                     decision = true;
                  } else {
                     printf("Invalid input.\n");
                     printf("File exists. Replace file? (y/n): ");
                     fgets( ans, MAXANSWERSIZE, stdin);
                     while (getchar() != '\n') {}
                  }
               }
               fclose(targetFile);
            }
            if (cancel) continue;
            else        break;
			case 'q':
				quit = true;
            break;
			case 'h':
				printf("l: List\n");
				printf("c: Check <file name>\n");
				printf("p: Display <file name>\n");
				printf("d: Download <file name>\n");
				printf("q: Quit\n");
				printf("h: Help\n");
				continue;
			default:
				printf("ERROR: entered command does not exits (enter 'h' for help)\n");
            continue;
		}
		if (quit) break;
      
		memset(&hints, 0, sizeof hints);
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;

		if ((rv = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0) {
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
			return 1;
		}

		// loop through all the results and connect to the first we can
		for(p = servinfo; p != NULL; p = p->ai_next) {
			if ((sockfd = socket(p->ai_family, p->ai_socktype,
					p->ai_protocol)) == -1) {
				perror("client: socket");
				continue;
			}

			if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
				close(sockfd);
				perror("client: connect");
				continue;
			}

			break;
		}

		if (p == NULL) {
			fprintf(stderr, "client: failed to connect\n");
			return 2;
		}

		inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
				s, sizeof s);
		printf("client: connecting to %s\n", s);

		// send command message (command then filename)
		if (send(sockfd, request, MAXDATASIZE-1, 0) == -1)
					perror("send");
		if (send(sockfd, filename, MAXDATASIZE-1, 0) == -1)
					perror("send");
			
		freeaddrinfo(servinfo); // all done with this structure

      // get and print contents of file for list() and display()
      if ((request[0] == 'l') || (request[0] == 'p')) {
		   do {
		      if ((numbytes = recv(sockfd, response, MAXDATASIZE-1, 0)) == -1) {
			      perror("recv");
			      exit(1);
		      }
		      response[numbytes] = '\0';	
		
		      printf("%s", response);
			
	      } while( numbytes != 0 );
      }

      // wait for server to process if command is check
      if (request[0] == 'c') {
         if (!fork()) { // child to execute sleep
            execlp("/bin/sleep", "sleep", "1", (char*)NULL);
            exit(0);
         }
         while( wait(NULL) > 0 );   // wait for child process to finish
      }
      
      // save contents of file for download()
      if (request[0] == 'd') {
         // open file
         newFile = fopen(path, "w");
         if (!newFile) {
            perror("File opening failed");
            exit(1);
         }
		   
         do {
		      if ((numbytes = recv(sockfd, response, MAXDATASIZE-1, 0)) == -1) {
			      perror("recv");
			      exit(1);
		      }
		      response[numbytes] = '\0';	
		      
		      fputs(response, newFile);
	      } while( numbytes != 0 );

         free(path);
         fclose(newFile);
      }

      close(sockfd);
	}

	return 0;
}


