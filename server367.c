/*
** server.c -- a stream socket server demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#define PORT "3523"  // the port users will be connecting to

#define BACKLOG 10	 // how many pending connections queue will hold

#define MAXDATASIZE 100 // max number of bytes we can get at once 

void sigchld_handler(int s)
{
	while(waitpid(-1, NULL, WNOHANG) > 0);
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
// converts message recieved from lower to uppercase
void lowercaseTOupper( int new_fd ) {
	char string[MAXDATASIZE];
   int numbytes;
   
   // recieve a string
   if ((numbytes = recv(new_fd, string, MAXDATASIZE-1, 0)) == -1) {
      perror("recv");
	   exit(1);
	}
   string[numbytes] = '\0';
	printf("server: received '%s'\n",string);
         
   // create return message
   for( int i=0; string[i] != '\0'; i++) {
      string[i] = string[i] - 32;
      //printf("%c\n", string[i]);
   }

	if (send(new_fd, string, MAXDATASIZE-1, 0) == -1)
		perror("send");
	close(new_fd);
	exit(0);
}

void list( int new_fd ) {
	char reply[MAXDATASIZE];
   
   if (!fork()) { // child to execute ls
      //execlp("/bin/ls", "ls", (char*)NULL);
      execlp("/bin/bash", "bash", "lsOutput.sh", (char*)NULL);
      exit(0);
   }
   while( wait(NULL) > 0 );   // wait for child process to finish
         
   // send output to client
   FILE* lsOut = fopen("lsOutput.log", "r");
   if (!lsOut) {
      perror("File opening failed");
      exit(1);
   }
   while( fgets(reply, sizeof reply, lsOut) ) {
      // send contents of lsOutput.log
      if (send(new_fd, reply, MAXDATASIZE-1, 0) == -1)
			perror("send");
   }
   fclose(lsOut);
         
   if (!fork()) { // child to execute remove lsOutput.log
      execlp("/bin/rm", "rm", "lsOutput.log", (char*)NULL);
      exit(0);
   }
   while( wait(NULL) > 0 );   // wait for child process to finish
}

void check( int new_fd, char filename[] ) {
   if (access(filename, F_OK) == (-1))
      printf("File '%s' not found\n", filename);
   else
      printf("File '%s' found\n", filename); 
}

void display( int new_fd, char filename[] ) {
   if (access(filename, F_OK) == (-1))
      printf("File '%s' not found\n", filename);
   else {
      //printf("DEBUG: File '%s' found\n", filename);  
	   char reply[MAXDATASIZE];
   
      // open file
      FILE* fp = fopen(filename, "r");
      if (!fp) {
         perror("File opening failed");
         exit(1);
      }
      while( fgets(reply, sizeof reply, fp) ) {
         // send contents of file
         if (send(new_fd, reply, MAXDATASIZE-1, 0) == -1)
			   perror("send");
      }
      fclose(fp);
   }
}

void download( int new_fd, char filename[] ) {
   if (access(filename, F_OK) == (-1))
      printf("File '%s' not found\n", filename);
   else {
      //printf("DEBUG: File '%s' found\n", filename);  
	   char reply[MAXDATASIZE];
   
      // open file
      FILE* fp = fopen(filename, "r");
      if (!fp) {
         perror("File opening failed");
         exit(1);
      }
      while( fgets(reply, sizeof reply, fp) ) {
         // send contents of file
         if (send(new_fd, reply, MAXDATASIZE-1, 0) == -1)
			   perror("send");
      }
      fclose(fp);
   }
}

int main(void)
{
	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size;
	struct sigaction sa;
	int yes=1;
	char s[INET6_ADDRSTRLEN];
	int rv;

   int numbytes;
	
   memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}

	if (p == NULL)  {
		fprintf(stderr, "server: failed to bind\n");
		return 2;
	}

	freeaddrinfo(servinfo); // all done with this structure

	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	sa.sa_handler = sigchld_handler; // reap all dead processes
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction");
		exit(1);
	}

	printf("server: waiting for connections...\n");

	while(1) {  // main accept() loop
		sin_size = sizeof their_addr;
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (new_fd == -1) {
			perror("accept");
			continue;
		}

		inet_ntop(their_addr.ss_family,
			get_in_addr((struct sockaddr *)&their_addr),
			s, sizeof s);
		printf("server: got connection from %s\n", s);

		if (!fork()) { // this is the child process
			close(sockfd); // child doesn't need the listener
         
         //get request
         char request[MAXDATASIZE];
         char filename[MAXDATASIZE];
         int numbytes;

         // recieve command
         if ((numbytes = recv(new_fd, request, MAXDATASIZE-1, 0)) == -1) {
            perror("recv");
	         exit(1);
	      }
         request[numbytes] = '\0';
	      printf("server: received '%s'\n",request);
         // recieve filename
         if ((numbytes = recv(new_fd, filename, MAXDATASIZE-1, 0)) == -1) {
            perror("recv");
	         exit(1);
	      }
         request[numbytes] = '\0';
	      printf("server: received '%s'\n",filename);
   
         // process request
         switch (request[0]) {
         case 'l':
            list( new_fd );
            break;
         case 'c':
            check( new_fd, filename );
            break;
         case 'p':
            display( new_fd, filename );
            break;
         case 'd':
            download( new_fd, filename );
            break;
         default:
            printf("ERROR: command does not exits\n");
            break;
         }
	      close(new_fd);
	      exit(0);
		}
		close(new_fd);  // parent doesn't need this
	}
	return 0;
}

